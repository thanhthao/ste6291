
#include "Rock.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"


Rock::Rock()
{

    glm::vec3 vec = glm::vec3(250.0f, -90.0f, -3000.0f);
    matrix_ = glm::translate(matrix_,vec );
    pos_    = vec;


}

Rock::~Rock()
{
}

void Rock::privateInit()
{

      s_.initShaders("./shaders/phong");

    listId_ = glGenLists(1);
    glNewList(listId_, GL_COMPILE);

    GLUquadric *quad;
    quad = gluNewQuadric();
    gluSphere(quad,25,200,50);

    glEndList();


}

void Rock::privateRender()
{
     s_.enable();

     glEnable(GL_LIGHTING);

     glCallList(listId_);

     glDisable(GL_LIGHTING);

     s_.disable();
}

void Rock::privateUpdate()
{


}


glm::vec3 Rock::getPos()
{
    return pos_;
}

void Rock::setPos(const glm::vec3 &pos){

   matrix_ = glm::translate(matrix_,pos );
   pos_= pos;
}


