#ifndef _ROCK_HPP_
#define _ROCK_HPP_

#include "Shader.hpp"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include "SceneObject.hpp"


class Rock : public SceneObject
{
public:
    Rock();
    ~Rock();
    glm::vec3 getPos() ;
    void setPos(const glm::vec3 &pos_);

protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

private:

    int listId_;
    glm::vec3 pos_;
    Shader s_;

};
#endif
