
#include "SpaceShip.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "SOIL.h"
#include <iostream>
#include <GL/freeglut.h>


SpaceShip::SpaceShip()
{
    camera_pos = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 vec = glm::vec3(0.0f, 0.0f, -300.0f);
    matrix_ = glm::translate(matrix_,vec );
    pos_    = vec; // tai sao ko hien hinh

    radius_ = 10.0f;
    life_   = 10.0f; // bi ban trung 2 lan moi chet???
    score_  = 0;

}

SpaceShip::~SpaceShip()
{
}

void SpaceShip::privateInit()
{

   texture_ = SOIL_load_OGL_texture("./images/metal2.jpg",SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    if( texture_== 0 ) printf( "SOIL loading error in spaceship : '%s'\n", SOIL_last_result() );

    std::string str1;
    str1= "./shaders/reflect";
    const char * c1 = str1.c_str();
    s_.initShaders(c1);

    std::string str2;
    str2= "./model/spaceship/gunship_3DS.3DS";

    const char * c2 = str2.c_str();
    load_.Import3DS(&model_, c2);

    glGenTextures(1, &cubeMap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);


    s_.enable();

    location_ = glGetUniformLocation(s_.getProg(), "cubeMap"); //cubeMap : ten cua texture, bind it
    glUniform1i(location_,0)   ;                             // cubeMap phai la skybox_ in Skybox.cpp

    s_.disable();



}

void SpaceShip::privateRender()
{
    s_.enable();

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP,cubeMap);

    glFrontFace(GL_CW);
    glDisable(GL_DEPTH_TEST);

    // load 3ds
    load_.loadModel(model_,texture_);


    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_CUBE_MAP);

    glFrontFace(GL_CCW);
    glEnable(GL_DEPTH_TEST);

    s_.disable();


}


void SpaceShip::privateUpdate()
{
    s_.enable();
    location_ = glGetUniformLocation(s_.getProg(), "cubeMap"); //cubeMap : ten cua texture, bind it
    glUniform1i(location_,0)   ;

    location1_ = glGetUniformLocation(s_.getProg(), "camera_pos");
    glUniform3f (location1_, this->getCameraPos()[0],this->getCameraPos()[1],this->getCameraPos()[2])   ;

    s_.disable();
}

void SpaceShip::moveRight()
{

    if(pos_[0] >= -320.0f && pos_[0] < 320.0f) // not out of the battle field
    {
        glm::vec3 rightvec = glm::vec3(5.0f, 0.0f, 0.0f);
        matrix_ = glm::translate(matrix_,rightvec );
        pos_ += rightvec;
    }

}

void SpaceShip::moveLeft()
{

    if(pos_[0] > -320.0f && pos_[0] <= 320.0f)  //not out of the battle field
    {
        glm::vec3 leftvec = glm::vec3(-5.0f, 0.0f, 0.0f);
        matrix_ = glm::translate(matrix_,leftvec );
        pos_ += leftvec;

    }
}

void SpaceShip::moveForward()
{
     std::cout<<"spaceship pos before moving forward:" <<pos_[0] <<" "<<pos_[1]<<" "<<pos_[2]<<std::endl;


    if(pos_[2] <= -300.0f && pos_[2] > -5120.0f) // not out of the battle field
    {
        glm::vec3 upvec = glm::vec3(0.0f, 0.0f, -5.0f);
        matrix_ = glm::translate(matrix_,upvec );
        pos_ += upvec;
        std::cout<<"spaceship pos after moving forward:" <<pos_[0] <<" "<<pos_[1]<<" "<<pos_[2]<<std::endl;

    }

}
void SpaceShip::moveBack()
{
    // std::cout<<"spaceship pos before moving down:" <<pos_[0] <<" "<<pos_[1]<<" "<<pos_[2]<<std::endl;

    if(pos_[2] < -300.0f && pos_[2] >= -5120.0f) // not out of the battle field
    {
        glm::vec3 downvec = glm::vec3(0.0f, 0.0f,5.0f);
        matrix_ = glm::translate(matrix_,downvec );
        pos_ += downvec;
        // std::cout<<"spaceship pos after moving down:" <<pos_[0] <<" "<<pos_[1]<<" "<<pos_[2]<<std::endl;
    }

}
glm::vec3 SpaceShip::getPos()
{
    return pos_;

}
void SpaceShip::setPos( const glm::vec3 &new_pos)
{

    matrix_ = glm::translate(matrix_,new_pos);
    pos_+= new_pos;
    std::cout<<"spaceship in setPos():" <<pos_[0] <<" "<<pos_[1]<<" "<<pos_[2]<<std::endl;

}

void SpaceShip::setLife(float l1)
{
    life_ = l1;
}
float SpaceShip::getRadius(){

    return radius_;
}
float SpaceShip::getLife(){

    return life_;
}

glm::vec3 SpaceShip::getCameraPos(){

    return camera_pos;
    std::cout<<"pos3: "<<camera_pos[0]<<" "<<camera_pos[1]<<" "<<camera_pos[2]<<std::endl;


}
void SpaceShip::setCameraPos(const glm::vec3 &pos){

    camera_pos = pos;
    //  std::cout<<"pos1: "<<pos[0]<<" "<<pos[1]<<" "<<pos[2]<<std::endl;
    //  std::cout<<"pos2: "<<camera_pos[0]<<" "<<camera_pos[1]<<" "<<camera_pos[2]<<std::endl;


}

void SpaceShip::setSkyboxTexture(GLuint texture){

    cubeMap=texture;
}


