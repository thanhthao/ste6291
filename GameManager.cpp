
#include "GameManager.hpp"
#include <iostream>

GameManager::GameManager()
{
    die1_ = 0;
    die2_ = 0;
    die3_ = 0;

}

GameManager::~GameManager()
{
}

void GameManager::privateInit()
{
    // Set default OpenGL states
    glEnable(GL_CULL_FACE);

    // Adding the camera to the scene
    cam_.reset(new Camera());
    //  this->addSubObject(cam_);
    //  matrix_.translate(0.0f, 0.0f, -99.0f);

    sky_.reset(new Skybox());
    this->addSubObject(sky_);

    bf_.reset(new BattleField());
    this->addSubObject(bf_);

    water_.reset(new Water());
    this->addSubObject(water_);

    water2_.reset(new Water());
    this->addSubObject(water2_);
    water2_->setPos(280);

    spaceship_.reset(new SpaceShip());
    this->addSubObject(spaceship_);

    rock_.reset(new Rock());
    this->addSubObject(rock_);

    rock2_.reset(new Rock());
    this->addSubObject(rock2_);
    rock2_->setPos(glm::vec3(70.0f, 0.0f, 0.0f));

    rock3_.reset(new Rock());
    this->addSubObject(rock3_);
    rock3_->setPos(glm::vec3(90.0f, 0.0f, 20.0f));

    rock4_.reset(new Rock());
    this->addSubObject(rock4_);
    rock4_->setPos(glm::vec3(-530.0f, 0.0f, 1450.0f));

    rock5_.reset(new Rock());
    this->addSubObject(rock5_);
    rock5_->setPos(glm::vec3(-480.0f, 0.0f, 1450.0f));
 }

void GameManager::addEnemy()
{
    std::shared_ptr<Enemy> enemy_;
    enemy_.reset(new Enemy());
    this->addSubObject(enemy_);
    enemyArray_.push_back(enemy_);
    addBulletEnemy();


}

void GameManager::privateRender()
{

    glm::mat4 mat = cam_->getMatrix();
    glm::vec3 m = glm::vec3(-mat[3][0],-mat[3][1],-mat[3][2]);
    sky_->setPosition(m);

    spaceship_->setCameraPos(m);
    spaceship_->setSkyboxTexture(sky_->getCubeMap()); // lay texture of skybox and get for spacship to reflect the scene


    // remove bullet of spaceship when it out of the battlefield
    for(int i = 0; i < bulletSsArray_.size();i++)
        if(bulletSsArray_[i]->getPos()[2] < -5120.0f)
        {
            this->removeSubObject(bulletSsArray_[i]);
            bulletSsArray_.erase(bulletSsArray_.begin()+i);
            //i--;                                          // i-- de ko bi hut khi xoa ptu tieptheo
        }

    // remove the bullet of enemies when it is out of the battlefield
    for(int i = 0; i < bulletEnemyArray_.size();i++)
        if(bulletEnemyArray_[i]->getPos()[2] > 10.0f)
        {
            this->removeSubObject(bulletEnemyArray_[i]);
            bulletEnemyArray_.erase(bulletEnemyArray_.begin()+i);
            //i--;
        }

    // remove the enemies when it is out of the battlefield
    for(int i = 0; i < enemyArray_.size();i++)
        if(enemyArray_[i]->getPos()[2] > 10.0f)
        {
            this->removeSubObject(enemyArray_[i]);
            enemyArray_.erase(enemyArray_.begin()+i);
            //i--;
        }

//     1.when spaceship touches the enemy:
//     set spaceship: life = life - 10 or -20 or -30 depends on the type of enemy
//     and this enemy will die
    for(int i = 0; i < enemyArray_.size();i++)
    {
        glm::vec3 pos1 = spaceship_->getPos();
        glm::vec3 pos2 = enemyArray_[i]->getPos();
        float r1 = spaceship_->getRadius();
        float r2 = enemyArray_[i]->getRadius();

        if(checkCollision(pos1,pos2,r1,r2))
        {
            std::cout<<"spaceship touchs the enemy"<<std::endl;

            this->removeSubObject(enemyArray_[i]);
            enemyArray_.erase(enemyArray_.begin()+i);
            //i--;
            // checking score of spaceship, just minus when it is >0
            if(spaceship_->getLife() >= 0.0f){
                if(r2 == 20.0f)
                    spaceship_->setLife(spaceship_->getLife() - 10.0f);
                else if(r2 == 25.0f)
                    spaceship_->setLife(spaceship_->getLife() - 20.0f);
                else if(r2 == 30.0f)
                    spaceship_->setLife(spaceship_->getLife() - 30.0f);
            }
            //check the spaceship
            if (spaceship_->getLife() <= 0 ){
                this->removeSubObject(spaceship_);

            }

        }

    }
//     2.spaceship shoots the enemy
//     + using small bullet (life < 300):
//     set life of small enemy  : life = life - 5
//                 medium enemy : life = life - 5
//                 big enemy    : life = life - 5
//     then check enemy: life <= 0 , enemy dies
//     set spaceship :life = life + 10 or 15 or 20 when enemy dies
//     it means: small enemy  :is shooted 1 time => die
//               medium enemy :           2 times => die
//               big enemy    :           3 times => die

//     + using medium bullet (300 <= life <500):
//     set life of small enemy  : life = life - 5
//                 medium enemy : life = life - 10
//                 big enemy    : life = life - 7.5
//     then check enemy: life <= 0 , enemy dies
//     set spaceship :life = life + 10 or 15 or 20 when enemy dies
//     it means: small enemy  :is shooted 1 time => die
//               medium enemy :           1 times => die
//               big enemy    :           2 times => die

//     + using big bullet (life >= 500):
//     set life of small enemy  : life = life - 5
//                 medium enemy : life = life - 10
//                 big enemy    : life = life - 15
//     then check enemy: life <=0 , enemy dies
//     set spaceship :life = life + 10 or 15 or 20 when enemy dies
//     it means: small enemy  :is shooted 1 time => die
//               medium enemy :           1 times => die
//               big enemy    :           1 times => die


    for (int i = 0; i < enemyArray_.size(); i++){

        for (int j = 0; j < bulletSsArray_.size(); j++){

            glm::vec3 pos1 = bulletSsArray_[j]->getPos();
            glm::vec3 pos2 = enemyArray_[i]->getPos();
            float r1 = bulletSsArray_[j]->getRadius();
            float r2 = enemyArray_[i]->getRadius();
            int bullet_type = bulletSsArray_[j]->getType();

            if (checkCollision(pos1,pos2,r1,r2))
            {
                std::cout<<"spaceship's bullet touchs enemy"<<std::endl;
                std::cout<<"enemy radius: "<<r2<<std::endl;
                std::cout<<"enemy pos: "<<pos2[0]<< " "<<pos2[1]<<" "<<pos2[2]<<std::endl;

                this->removeSubObject(bulletSsArray_[j]); // delete the bullet of spaceship
                bulletSsArray_.erase(bulletSsArray_.begin()+j);
                //j--;

                // update value of enemy

                if(r2 == 20.0f) // small enemy

                    enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 5.0f);

                else if(r2 == 25.0f) //medium enemy
                {
                    if(bullet_type == 4)  //small bullet
                        enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 5.0f);
                    else if(bullet_type == 5|| bullet_type == 6) // medium or big bullet
                        enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 10.0f);
                    else if(bullet_type == 7)
                        enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 10.0f);
                    else if(bullet_type == 8)
                        enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 0.0f);

                }
                else if(r2 == 30.0f)  //big enemy
                {
                    if(bullet_type == 4) // small bullet
                        enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 5.0f);
                    else if(bullet_type == 5) //medium bullet
                        enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 7.5f);
                    else if(bullet_type == 6) //big bullet
                        enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 15.0f);
                    else if(bullet_type == 7)
                        enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 0.0f);
                    else if(bullet_type == 8)
                        enemyArray_[i]->setLife(enemyArray_[i]->getLife()- 15.0f);
                }

                if (enemyArray_[i]->getLife() <= 0) {                       // delete enemy when its life <=0

                    if(enemyArray_[i]->getRadius()== 20.0f)
                    {
                        die1_++;                                            // to count the number of enemy dies for each type
                        spaceship_->setLife(spaceship_->getLife() + 10.0f);  //increase the value of life
                    }
                    else if(enemyArray_[i]->getRadius()== 25.0f)
                    {
                        die2_++;
                        spaceship_->setLife(spaceship_->getLife() + 15.0f); //increase the value of life

                    }
                    else if(enemyArray_[i]->getRadius()== 30.0f)
                    {
                        die3_++;
                        spaceship_->setLife(spaceship_->getLife() + 20.0f);//increase the value of life
                    }
                    this->removeSubObject(enemyArray_[i]);  //delete
                    enemyArray_.erase(enemyArray_.begin()+i);
                  // i--;
                }

            }
            // check the spaceship
            if (spaceship_->getLife() <= 0 ){
                this->removeSubObject(spaceship_);
            }

        }


    }


    //3. enemy shoots spaceship, spaceship : life = life - 5 or -10 or -15

    for(int i = 0; i < bulletEnemyArray_.size();i++)
    {
        glm::vec3 pos1 = spaceship_->getPos();
        glm::vec3 pos2 = bulletEnemyArray_[i]->getPos();
        float r1 = spaceship_->getRadius();
        float r2 = bulletEnemyArray_[i]->getRadius();

        if(checkCollision(pos1,pos2,r1,r2))
        {
            std::cout<<"enemy shoots spaceship"<<std::endl;

            int type = bulletEnemyArray_[i]->getType();
            this->removeSubObject(bulletEnemyArray_[i]);
            bulletEnemyArray_.erase(bulletEnemyArray_.begin()+i);
           // i--;
            //  checking score of spaceship, just minus when it is >0
            if(spaceship_->getLife() >= 0.0f){

                spaceship_->setLife(spaceship_->getLife()-5.0f*type);
            }
        }

        // check the spaceship
        if (spaceship_->getLife() <= 0 ){
            this->removeSubObject(spaceship_);

        }

    }

}

void GameManager::privateUpdate()
{
    // Instead of adding alle objects in the scene as subobject to the camera they are added as subobjects
    // to the game manager. Therefore, we set the game manager matrix equal to the camera matrix.
    this->matrix_ = cam_->getMatrix();
    // sky_->setMatrix(cam_->getMatrix());
}

std::shared_ptr<Camera> GameManager::getCam()
{
    return cam_;
}
std::shared_ptr<SpaceShip> GameManager::getSpaceShip()
{
    return spaceship_;
}

void GameManager::addBulletEnemy(){

    for (int i = 0; i < enemyArray_.size(); i++){

        std::shared_ptr<Bullet> bullet_;
        glm::vec3 dir_(0.0f,0.0f,5.0f);

        //enemy 1- no bullet
        //enemy 2 -bullet color: red
        //enemy 3 -bullet color: gray
        if (enemyArray_[i]->getRadius() == 25.0f){
            bullet_.reset(new Bullet());
            glm::vec3 pos1 = enemyArray_[i]->getPos();
            bullet_->setPos(pos1);
            bullet_->setColor(1.0f,0.0f,0.0f);
            bullet_->setType(2);
            bullet_->setDir(dir_);
            this->addSubObject(bullet_);
            bulletEnemyArray_.push_back(bullet_);
        }
        else if (enemyArray_[i]->getRadius() == 30.0f)
        {
            bullet_.reset(new Bullet());
            glm::vec3 pos2 = enemyArray_[i]->getPos();
            bullet_->setPos(pos2);
            bullet_->setColor(0.8f,0.8f,0.8f);
            bullet_->setType(3);
            bullet_->setDir(dir_);
            this->addSubObject(bullet_);
            bulletEnemyArray_.push_back(bullet_);
        }

    }

}

void GameManager::addBulletSs(const int number){

    std::shared_ptr<Bullet> bullet_;
    glm::vec3 dir_(0.0f,0.0f,-10.0f);
    bullet_.reset(new Bullet());
    bullet_->setPos(spaceship_->getPos());

    switch(number){
    case 1: // phim a
    {
        bullet_->setColor(0.0f,1.0f,1.0f);//white blue
        bullet_->setType(7);

    }
        break;
    case 2:// phim s
    {
        if(spaceship_->getLife() < 300.0f){

            bullet_->setColor(0.0f,0.0f,0.0f); //black
            bullet_->setType(4);
        }
        else if (spaceship_->getLife() >= 300.0f && spaceship_->getLife() < 500.0f ){

            bullet_->setColor(0.0f,0.0f,0.0f);
            bullet_->setType(5);
        }

        else{

            bullet_->setColor(0.0f,0.0f,0.0f);
            bullet_->setType(6);
        }


    }
        break;


    case 3: // phim d
    {
        bullet_->setColor(0.0f,1.0f,0.0f); //green
        bullet_->setType(8);

    }
        break;

    }

    bullet_->setDir(dir_);
    this->addSubObject(bullet_);
    bulletSsArray_.push_back(bullet_);

}


bool GameManager::checkCollision(glm::vec3 pos1, glm::vec3 pos2, float r1, float r2){

    float length = glm::length(pos1 - pos2); // distance between 2 centers.
    if ( r1 + r2 >= length)
        return true;

    return false;
}

//show text with its length  and the length of the score at position(x,y)
//print score in big size
void GameManager::showText(const char *text, int length, int length_score, int x, int y)
{
    glColor3f(1.0f, 0.0f, 0.0f); //red
    glMatrixMode(GL_PROJECTION);
    double *matrix = new double[16];
    glGetDoublev(GL_PROJECTION_MATRIX, matrix);
    glLoadIdentity();
    glOrtho(0,700,0,700,-5,5);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i(x,y);
    // print small text
    for(int i = 0; i<length-length_score;i++)
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,(int)text[i]);
    //print big score text
    for(int j = length-length_score; j<length;j++)
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,(int)text[j]);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixd(matrix);
    glMatrixMode(GL_MODELVIEW);
}

int GameManager::getDie1(){
    return die1_;
}

int GameManager::getDie2(){
    return die2_;
}

int GameManager::getDie3(){
    return die3_;
}
