#ifndef FEMAPPLICATION_H
#define FEMAPPLICATION_H

#include "node.h"


class FEMApp:public GMlib::TriangleFacets<float>{


private:
    GMlib::ArrayLX<Node>  node_array;  // array of internal nodes

    GMlib::DMatrix<float> A , A1;      // stiffness matrix and invert

    GMlib::DVector<float> b;           //load vector

    double update;
    bool move_up;                     //check it move up or down

public:
   FEMApp( int d = 0);
   FEMApp(const GMlib::ArrayLX<GMlib::TSVertex<float> >& v);
   void regularNode(int m, int n, float radius);  //m: number of level (circle)
                                                  //n: number of points in the first circle
   void randomNode(int k,float radius);           //k: number of points in the boundary
   void internalNode();
   void computeMatrix();
   void computeLoadVector();
   GMlib::Vector<GMlib::Vector<float,2> ,3> findVectors(GMlib::TSEdge<float> *edge);
   GMlib::Vector<GMlib::Vector<float,2> ,3> findVectors(Node &n,GMlib::TSTriangle<float> *triangle);
   float findElement(GMlib::TSEdge<float>* edge);
   void updateNode(double d);
   bool pointOK(GMlib::Point<float,2> pt,GMlib::ArrayLX<GMlib::TSVertex<float > > vt, float min_dist);

protected:
   void localSimulate(double dt);

};




#endif // FEMAPPLICATION_H
