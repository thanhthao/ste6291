-- 3dvia.com   --

The zip file space fighter.obj.zip contains the following files :
- readme.txt
- space fighter.obj
- bottomexhaust-paint-05.gif
- bottomwing-paint-01.gif
- dagger-hull-paint24.gif
- gunport-paint-02.gif
- intake-paint-03.gif
- upperexhaust-paint-06.gif
- space fighter.mtl


-- Model information --

Model Name : space fighter
Author : Mansoor Zaidi
Publisher : mansoorz

You can view this model here :
http://www.3dvia.com/content/1FFB031527390B1D
More models about this author :
http://www.3dvia.com/mansoorz


-- Attached license --

A license is attached to the space fighter model and all related media.
You must agree with this licence before using the enclosed media.

License : Attribution-NonCommercial-ShareAlike 2.5
Detailed license : http://creativecommons.org/licenses/by-nc-sa/2.5/

The licenses used by 3dvia are based on Creative Commons Licenses.
More info: http://creativecommons.org/about/licenses/meet-the-licenses
