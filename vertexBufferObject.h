#pragma once

#include <GL/glu.h>
#include <GL/gl.h>

#include<vector>
/********************************

Class:	CVertexBufferObject

Purpose:	Wraps OpenGL vertex buffer
			object.

********************************/

class CVertexBufferObject
{
public:
	void createVBO(int a_iSize = 0);
	void releaseVBO();

	void* mapBufferToMemory(int iUsageHint);
	void* mapSubBufferToMemory(int iUsageHint, UINT uiOffset, UINT uiLength);
	void unmapBuffer();

	void bindVBO(int a_iBufferType = GL_ARRAY_BUFFER);
	void uploadDataToGPU(int iUsageHint);
	
	void addData(void* ptrData, UINT uiDataSize);

	void* getDataPointer();
	UINT getBuffer();

	CVertexBufferObject();

private:
	UINT uiBuffer;
	int iSize;
	int iBufferType;
    std::vector<BYTE> data;

	bool bDataUploaded;
};
