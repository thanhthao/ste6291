#pragma once

#include "Shader.hpp" //phai dat truoc
#include <windows.h>
#include "GL/glew.h"
#include "GL/freeglut.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include "glm/glm.hpp"

class Tree : public SceneObject
{
public:
    Tree();
    ~Tree();
    void tree(float x1, float y1, float length1, float angle1, int depth);
    float myrand(float R);
protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

private:
    std::vector< glm::vec3 > vertexArray_; // Maybe two-dim vector and several arrays
    std::vector< unsigned int> indexArray_; // index arrays
    // normal array.

    std::vector<glm::vec2> texCoordArray_; // texture coord array
    GLuint texture_id;
    Shader s;
//    GLint texSampler;
//    GLfloat water_time;
//    GLint waveTimeLoc;
};

