#pragma once

const int KEY_ID_W        = 0;
const int KEY_ID_S        = 1;
const int KEY_ID_A        = 2;
const int KEY_ID_D        = 3;
const int KEY_ID_SPACE    = 4;
const int KEY_ID_C        = 5;
const int KEY_ID_RIGHT    = 6;
const int KEY_ID_LEFT     = 7;
const int KEY_ID_UP       = 8;
const int KEY_ID_DOWN     = 9;



const int MOUSE_LEFT_BUTTON_DOWN = 20;
