
#include <windows.h>
#include "GL/glew.h"     // moi them sau,ko duoc doi xuong
#include <GL/freeglut.h>
#include <stdlib.h>
#include <stdio.h>
#include "Input.hpp"
#include "FpsCounter.hpp"
#include "GameManager.hpp"
#include "glm/glm.hpp"
#include <iostream>
#include <sstream>  // ostreamstring ...


std::shared_ptr<GameManager> gm;
siut::FpsCounter counter;

int window;

bool keyPressed[30];
int mousePosX, mousePosY; 
float moveX, moveY;

void init()
{
    GLenum glew_status = glewInit();

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);

    counter.start();

    gm.reset(new GameManager());
    gm->init();

    for(int i=0; i<30; i++)
        keyPressed[i]=false;
    glEnable(GL_NORMALIZE);
}


void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    gm->update(counter.fps());
    gm->render();

    //camera and spaceship move together
    if(keyPressed[KEY_ID_UP]==true )
    {

        if(gm->getCam()->getPos()[2] < 3000.0f)
        {
            gm->getSpaceShip()->moveForward();
            gm->getCam()->moveForward();
            keyPressed[KEY_ID_UP]==false;
        }
        else {

            gm->getSpaceShip()->setPos(glm::vec3(0.0f,0.0f,3315.0f)); // old: 3315 voi -300
            gm->getCam()->setPos(glm::vec3(0.0f, 0.0f, -3320.0f));    //3320= 3000+320
            keyPressed[KEY_ID_UP]==false;
        }

    }

    if(keyPressed[KEY_ID_DOWN]==true)
    {
        gm->getCam()->moveBackward();
        gm->getSpaceShip()->moveBack();
        keyPressed[KEY_ID_DOWN]==false;
    }

    /*if(keyPressed[KEY_ID_SPACE]==true)
    {
        gm->getCam()->moveUp();
        keyPressed[KEY_ID_SPACE]==false;
    }

    if(keyPressed[KEY_ID_C]==true)
    {
        gm->getCam()->moveDown();
        keyPressed[KEY_ID_C]==false;
    }
    */

    // MOVE THE SPACESHIP
    if(keyPressed[KEY_ID_RIGHT]==true)
    {
        gm->getSpaceShip()->moveRight();
        keyPressed[KEY_ID_RIGHT]==false;
    }

    if(keyPressed[KEY_ID_LEFT]==true)
    {
        gm->getSpaceShip()->moveLeft();
        keyPressed[KEY_ID_LEFT]==false;
    }

    if(keyPressed[KEY_ID_S]==true && gm->getSpaceShip()->getLife() > 0.0f)
    {
        gm->addBulletSs(2);
        keyPressed[KEY_ID_S]=false;
    }

    if(keyPressed[KEY_ID_A]==true && gm->getSpaceShip()->getLife() > 0.0f)
    {
        gm->addBulletSs(1);
        keyPressed[KEY_ID_A]=false;
    }
    if(keyPressed[KEY_ID_D]==true && gm->getSpaceShip()->getLife() > 0.0f)
    {
        gm->addBulletSs(3);
        keyPressed[KEY_ID_D]=false;
    }

    glColor3f(1.0f,0.0f,0.0f);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    float life = gm->getSpaceShip()->getLife();

    std::ostringstream convert;
    convert << life;
    std::string text;
    text ="Your score is: "+ convert.str();
    gm->showText(text.data(), text.size(),convert.str().size(),100,580);

    int die1=gm->getDie1();
    std::ostringstream convert1;
    convert1 << die1;
    std::string text1;
    text1 ="Enemy type 1 dies: "+ convert1.str();
    gm->showText(text1.data(), text1.size(),convert1.str().size(),100,550);

    int die2=gm->getDie2();
    std::ostringstream convert2;
    convert2 << die2;
    std::string text2;
    text2 ="Enemy type 2 dies: "+ convert2.str();
    gm->showText(text2.data(), text2.size(),convert2.str().size(),100,520);

    int die3=gm->getDie3();
    std::ostringstream convert3;
    convert3 << die3;
    std::string text3;
    text3 ="Enemy type 3 dies: "+ convert3.str();
    gm->showText(text3.data(), text3.size(),convert3.str().size(),100,490);



    if (gm->getSpaceShip()->getLife() <= 0){
        std::string die;
        die = "Game Over!!! ";
        gm->showText(die.data(),die.size(),die.size(),300,400);
    }

    else if (gm->getSpaceShip()->getLife() == 1000.0f){

        std::string win;
        win = "You are winner!!! ";
        gm->showText(win.data(),win.size(),win.size(),300,400);
    }

    glutSwapBuffers();
    glutPostRedisplay();

}

void keyDown(unsigned char key, int x, int y)   // khi nhan phim
{
    switch (key)
    {
    case 'q':
    case 27:
        glutDestroyWindow(window);
#ifndef _WIN32
        // Must use this with regular glut, since it never returns control to main().
        exit(0);
#endif
        break;

    case 'w':
        keyPressed[KEY_ID_W] = true;
        break;
    case 'a':
        keyPressed[KEY_ID_A] = true;
        break;
    case 's':
        keyPressed[KEY_ID_S] = true;
        break;
    case 'd':
        keyPressed[KEY_ID_D] = true;
        break;
    case ' ':
        keyPressed[KEY_ID_SPACE] = true;
        break;
    case 'c':
        keyPressed[KEY_ID_C] = true;
        break;


    default:
        glutPostRedisplay();
    }
}


void keyUp(unsigned char key, int x, int y)   // ko con nhan phim
{
    switch (key)
    {
    case 'w':
        keyPressed[KEY_ID_W] = false;
        break;
    case 'a':
        keyPressed[KEY_ID_A] = false;
        break;
    case 's':
        keyPressed[KEY_ID_S] = false;
        break;
    case 'd':
        keyPressed[KEY_ID_D] = false;
        break;
    case ' ':
        keyPressed[KEY_ID_SPACE] = false;
        break;
    case 'c':
        keyPressed[KEY_ID_C] = false;
        break;


    }
}

void keyUpSpaceShip(int key, int x, int y){
    switch (key)
    {

    case GLUT_KEY_LEFT:
        keyPressed[KEY_ID_LEFT] = false;
        break;
    case GLUT_KEY_RIGHT:
        keyPressed[KEY_ID_RIGHT] = false;
        break;
    case GLUT_KEY_UP:
        keyPressed[KEY_ID_UP]=  false;
        break;
    case GLUT_KEY_DOWN:
        keyPressed[KEY_ID_DOWN]=  false;
        break;

    }

}
void keyDownSpaceShip(int key, int x, int y){
    switch (key)
    {

    case GLUT_KEY_RIGHT:
        keyPressed[KEY_ID_RIGHT] = true;
        break;
    case GLUT_KEY_LEFT:
        keyPressed[KEY_ID_LEFT] = true;
        break;
    case GLUT_KEY_UP:
        keyPressed[KEY_ID_UP]= true;
        break;
    case GLUT_KEY_DOWN:
        keyPressed[KEY_ID_DOWN]=  true;
        break;
    }

    glutPostRedisplay();

}


void mousePressed(int button, int state, int posX, int posY)
{
    if(button==GLUT_LEFT_BUTTON && state==GLUT_DOWN)
    {
        mousePosX = posX;
        mousePosY = posY;
        keyPressed[MOUSE_LEFT_BUTTON_DOWN] = true;
    }
    if(button==GLUT_LEFT_BUTTON && state==GLUT_UP)
        keyPressed[MOUSE_LEFT_BUTTON_DOWN] = false;
}

void mouseMoved(int posX, int posY)
{
    if(keyPressed[MOUSE_LEFT_BUTTON_DOWN])
    {
        int diffX = posX - mousePosX;
        mousePosX = posX;
        int diffY = posY - mousePosY;
        mousePosY = posY;

        // Implement quaternion based mouse move

    }
}

void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //  glOrtho(-50, 700, -50, 700, -50, 50);
    gluPerspective(60.0f, float(w)/float(h) ,0.5f, 5500.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //  gluLookAt(0.0, 0.0, 10.0,     0.0, 0.0, 0.0,    0.0, 1.0, 0.0);
}
void addEnemy1(int i){
    gm->addEnemy();

    glutTimerFunc(2000, addEnemy1, 0);
}


int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
    glutInitWindowSize(700, 700);
    glutInitWindowPosition(10, 10);
    window = glutCreateWindow("Space Shooter 3D");
    init();
    glutKeyboardFunc(keyDown);
    glutKeyboardUpFunc(keyUp);
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutMouseFunc(mousePressed);
    glutMotionFunc(mouseMoved);

    // Add other callback functions here..
    // move the spaceship
    glutSpecialFunc(keyDownSpaceShip);
    glutSpecialUpFunc(keyUpSpaceShip);
    glutTimerFunc(2000, addEnemy1, 0);





    glutMainLoop();
    return 0;
}
