#ifndef _BATTLEFIELD_HPP_
#define _BATTLEFIELD_HPP_

#include "Shader.hpp"   //must be in the top
#include <windows.h>
#include "GL/glew.h"
#include "GL/freeglut.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include "glm/glm.hpp"

class BattleField : public SceneObject
{
public:
    BattleField();
    ~BattleField();

protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

private:
    std::vector< glm::vec3 > vertexArray_;  // Maybe two-dim vector and several arrays
    std::vector< unsigned int> indexArray_; // index arrays
                                            // normal array.

    std::vector<glm::vec2> texCoordArray_;   // texture coord array
    GLuint texture_id1,texture_id2,texture_id3 ;
    Shader s1_;
    GLint texSampler;

};

#endif
