uniform sampler2D texture_id; // should be the same name with the texture
varying vec3 TexCoord;
void main() {
    // Set the output color of our current pixel
    gl_FragColor = texture2D(texture_id, TexCoord.xz);// not y
}
