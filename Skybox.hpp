#ifndef _SKYBOX_HPP_
#define _SKYBOX_HPP_

#include "Shader.hpp"
#include <windows.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include "SceneObject.hpp"
#include "glm/glm.hpp"


class Skybox : public SceneObject
{
public:
    Skybox();
    ~Skybox();
  void setPosition(const glm::vec3 &pos);
  GLuint getCubeMap();
    //

protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

private:
    float speed_;
    float life_;
    float armor_; // ao giap,huy chuong

    GLuint skybox_;
    Shader s_;
    GLint tex_;
    GLuint listId_;
    glm::vec3 pos_; //position

};

#endif
