#ifndef _CAMERA_HPP_
#define _CAMERA_HPP_

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include "SceneObject.hpp"
#include <vector>

class Camera : public SceneObject
{
public:
    Camera();
    ~Camera();

    void moveRight();
    void moveLeft();
    void moveUp();
    void moveDown();
    void moveBackward();
    void moveForward();
    void setPos(const glm::vec3 &new_pos);
    glm::vec3 getPos();

protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

private:

};

#endif
