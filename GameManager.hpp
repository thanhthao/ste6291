#ifndef _MANAGER_HPP_
#define _MANAGER_HPP_

#include <windows.h>
#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>

#include "SceneObject.hpp"
#include "BattleField.hpp"
#include "SpaceShip.hpp"
#include "Camera.hpp"
#include "Enemy.hpp"
#include "Bullet.hpp"
#include "Skybox.hpp"
#include "Water.hpp"
#include "Rock.hpp"


class GameManager : public SceneObject
{
public:
    GameManager();
    ~GameManager();
    
    std::shared_ptr<Camera> getCam();
    std::shared_ptr<SpaceShip> getSpaceShip();
    void addEnemy();
    void addBulletEnemy();
    void addBulletSs(const int number);
    bool checkCollision(glm::vec3 pos1, glm::vec3 pos2, float r1, float r2);
    void showText(const char *text, int length, int length_score, int x, int y);
    int getDie1();
    int getDie2();
    int getDie3();

protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

private:
    std::shared_ptr<BattleField> bf_;


    std::shared_ptr<SpaceShip> spaceship_;
    std::shared_ptr<Camera> cam_;

    std::vector< std::shared_ptr<Enemy> > enemyArray_;
    std::vector< std::shared_ptr<Bullet> > bulletEnemyArray_;
    std::vector< std::shared_ptr<Bullet> > bulletSsArray_;
    int die1_; // to count the number of each type of enemy that spaceship kills
    int die2_;
    int die3_;

    std::shared_ptr<Skybox> sky_;
    std::shared_ptr<Water> water_, water2_;
    std::shared_ptr<Rock> rock_,rock2_,rock3_,rock4_,rock5_;

};


#endif
