#ifndef _ENEMY_HPP_
#define _ENEMY_HPP_

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include "glm/glm.hpp"


class Enemy : public SceneObject
{
public:
    Enemy();
    ~Enemy();
    glm::vec3 getPos();
    float getRadius();
    float getLife();
    void setLife(float life);
    float getMass();
    glm::vec3 getDir();
    void setDir(const glm::vec3 &dir);
    void setLive(bool value);
    void setPos(const glm::vec3 &pos_new);
protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

private:
    float speed_;
    float life_;
    float armor_; // ao giap,huy chuong

    float angle_;
    glm::vec3 pos_;
    unsigned int list_id_;
    float radius_;
    int random_; // create random number for x axis
    float mass_;
    glm::vec3 dir_;
    bool live_;

};

#endif
