
#include "Bullet.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <iostream>
Bullet::Bullet()
{
    radius_ = 10.0f;
    r = 0.0f;
    g = 0.0f;
    b = 0.0f;
    type_ = 0;
    mass_=0;
   privateInit();
}

Bullet::~Bullet()
{
}

void Bullet::privateInit()
{

    // check type =1,2,3,4,5,6,7,8 to insert shape,color of bullet
    // 1,2,3 is of enemy
    // 4,5,6 is of spaceship key a
    // 7 is of spaceship key s
    // 8 is of spaceship key d

    mass_=0.5f;
    list_id_ = glGenLists(1);
    glNewList(list_id_, GL_COMPILE);

    if(type_ == 1 || type_== 2||type_== 3)
    {
        glBegin(GL_QUADS);
        glNormal3f( 0.0f, 1.0f, 0.0f );
        glColor3f(r,g,b);
        glVertex3f( pos_[0] + 3, pos_[1] - 3 + 5, pos_[2] ); // + 5 o truc y cho len bang spaceship
        glVertex3f( pos_[0] + 3, pos_[1] + 3 + 5, pos_[2] );
        glVertex3f( pos_[0] - 3, pos_[1] + 3 + 5, pos_[2] );
        glVertex3f( pos_[0] - 3, pos_[1] - 3 + 5, pos_[2] );

        glEnd();
    }
    else if(type_== 4 || type_== 5||type_== 6 || type_ == 7 ||type_== 8) {

       /* draw circle,it from internet */
        GLfloat R = 2.0f;
        glBegin(GL_LINE_LOOP);

        for(int i = 0; i < 360;i++)
        {
            glColor3f(r,g,b);
            glVertex3f(R*cos(2*3.14*i/360) + pos_[0],R*sin(2*3.14*i/360) + pos_[1]+10.0f ,pos_[2]);

        }
        glEnd();

    }

    glEndList();
}


void Bullet::privateRender()
{

    matrix_ = glm::translate(glm::mat4(), dir_);
    glCallList(list_id_);
    pos_ += dir_; // to move
    privateInit();

}

void Bullet::privateUpdate()
{

}

void Bullet::setPos(const glm::vec3 &pos2)
{
    pos_= pos2;
    privateInit();

}

glm::vec3 Bullet::getPos()
{
    return pos_;



}
void Bullet::setDir(const glm::vec3 &dir2)
{
    dir_ = dir2 ;
    privateInit();

}
void Bullet::setColor(GLfloat r1,GLfloat g1, GLfloat b1)
{
    r = r1;
    g = g1;
    b = b1;
    privateInit();
}
float Bullet::getRadius(){

    return radius_;
}

void Bullet::setType(int type){

    type_=type;
}

int Bullet::getType(){

    return type_;
}

glm::vec3 Bullet::getDir(){
    return dir_;
}

float Bullet::getMass(){
    return mass_;
}
