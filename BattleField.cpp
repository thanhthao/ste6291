#include "BattleField.hpp"
#include "glm/ext.hpp"  // to print matrix_
#include "glm/gtx/string_cast.hpp"
#include <GL/glut.h>
#include <iostream>
#include "SOIL/SOIL.h"

BattleField::BattleField()
{
}

BattleField::~BattleField()
{
}

void BattleField::privateInit()
{
     s1_.initShaders("./shaders/batlefield"); // name of shader


    glPrimitiveRestartIndex(32768);//= 64*512

    // color map

    texture_id1 = SOIL_load_OGL_texture
            (
                "./images/colorMap2012.bmp",
                SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y
                );
    if(texture_id1 == 0)
        std::cout<< "SOIL loading error in colorMap" ;

    int img_height1;
    int img_width1;
    unsigned char* img1 = SOIL_load_image("./images/colorMap2012.bmp", &img_width1, &img_height1, NULL, 0);

    glGenTextures(1, &texture_id1);
    glBindTexture(GL_TEXTURE_2D, texture_id1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA_FLOAT32_ATI, img_width1, img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, img1);


    //height map
    texture_id2 = SOIL_load_OGL_texture
            (
                "./images/mountains2.png",
                SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y
                );
    if(texture_id2 == 0)
        std::cout<< "SOIL loading error in height map" ;

    int img_height2;
    int img_width2;
    unsigned char* img2 = SOIL_load_image("./images/mountains2.png", &img_width2, &img_height2, NULL, 0);
    glGenTextures(1, &texture_id2);
    glBindTexture(GL_TEXTURE_2D, texture_id2);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA_FLOAT32_ATI, img_width2, img_height2, 0, GL_RGB, GL_UNSIGNED_BYTE, img2);

    // light map
    texture_id3 = SOIL_load_OGL_texture
            (
                "./images/lightMap2012.bmp",
                SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y
                );
    if(texture_id3 == 0)
        std::cout<< "SOIL loading error in lightmap" ;

    int img_height3;
    int img_width3;
    unsigned char* img3 = SOIL_load_image("./images/lightMap2012.bmp", &img_width3, &img_height3, NULL, 0);
    //set the texture
    glGenTextures(1, &texture_id3);
    glBindTexture(GL_TEXTURE_2D, texture_id3);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA_FLOAT32_ATI, img_width3, img_height3, 0, GL_RGB, GL_UNSIGNED_BYTE, img3);

      // Create vertex arrays
    int x_size = 64;
    int z_size = 512;

    glm::vec3 v;
    glm::vec2 t;

    for(int x = 0 ; x < x_size ; x ++){
        for(int z = 0 ; z < z_size ; z ++){

            v[0]= x*10.0f - 320.0f; //appear in  the middle of the screen -320
            v[1]= 0;
            v[2]= z*(-10.0f);
            t[0]= x/float(x_size-1);
            t[1]= z/float(z_size-1);
            vertexArray_.push_back(v);
            texCoordArray_.push_back(t);

        }
    }


    // create index array
    for(int x = 0 ; x < x_size - 1 ; x ++){
        for(int z = 0 ; z < z_size ; z ++){
            indexArray_.push_back(z + z_size*x);               // x =0: 0,1,2,3,4  cot 1
            indexArray_.push_back(z + z_size*x + z_size );     // x =0: 5,6,7,8,9  cot 2
        }

        indexArray_.push_back(32768);
    }

    indexArray_.pop_back();


      s1_.enable();

    texSampler = glGetUniformLocation(s1_.getProg(), "texture_id1");
    glUniform1i(texSampler, 0)   ;


    texSampler = glGetUniformLocation(s1_.getProg(), "texture_id2");
    glUniform1i(texSampler, 1)   ;


    texSampler = glGetUniformLocation(s1_.getProg(), "texture_id3");
    glUniform1i(texSampler, 2)   ;

    texSampler = glGetUniformLocation(s1_.getProg(), "blendFactor");
    glUniform1f(texSampler, 0.4)   ;

     s1_.disable();



}

void BattleField::privateRender()
{

    s1_.enable();

    // Render the battlefield
    glEnable(GL_PRIMITIVE_RESTART);

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture_id1);

    glActiveTexture(GL_TEXTURE1);
    glEnable(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, texture_id2);

    glEnableClientState(GL_VERTEX_ARRAY);                  // enable vertex arrays
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glTexCoordPointer(2, GL_FLOAT, 0, &texCoordArray_[0]);
    glVertexPointer(3, GL_FLOAT, 0, &vertexArray_[0]);     // set vertex pointer

    glDrawElements(GL_TRIANGLE_STRIP, indexArray_.size(), GL_UNSIGNED_INT,&indexArray_[0]); // connect points

    glDisableClientState(GL_VERTEX_ARRAY);                 // disable vertex arrays
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    glDisable(GL_PRIMITIVE_RESTART);


    glActiveTexture(GL_TEXTURE1);
    glDisable(GL_TEXTURE_2D);

    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);

    s1_.disable();

}

void BattleField::privateUpdate()
{
}

