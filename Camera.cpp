#include "Camera.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <iostream>

#define PIOVER180 0.0174532925199

Camera::Camera()
{
    matrix_ = glm::translate(glm::mat4(), glm::vec3(0.0f, -290.0f, -320.0f));
    // can chinh phia duoi nhung phai thay doi duoc spaceship
}

Camera::~Camera()
{
}

void Camera::privateInit()
{
}

void Camera::privateRender()
{
    // not drawing any camera geometry
}

void Camera::privateUpdate()
{

}

void Camera::moveRight()
{
    matrix_ = glm::translate(matrix_, glm::vec3(-5.0f, 0.0f, 0.0f));
    // print the matrix
    //    int i,j;
    //    for (j=0; j<4; j++){
    //        for (i=0; i<4; i++){
    //            printf("%f ",matrix_[i][j]);
    //        }
    //        printf("\n");
    //    }
}

void Camera::moveLeft()
{
    matrix_ = glm::translate(matrix_, glm::vec3(5.0f, 0.0f, 0.0f));
}

void Camera::moveUp()
{
    matrix_ = glm::translate(matrix_, glm::vec3(0.0f, -5.0f, 0.0f));

}

void Camera::moveDown()
{
    matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 5.0f, 0.0f));

}

void Camera::moveForward()
{
    glm::vec3 m = glm::vec3(matrix_[3][0],matrix_[3][1],matrix_[3][2]);

    // < 4135 de ko di chuyen ra khoi battlefield
    if(m[2] >= -320.0f && m[2] < 4135 ){ // 5120+650
        matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 5.0f));
    }

}

void Camera::moveBackward()
{
    glm::vec3 m = glm::vec3(matrix_[3][0],matrix_[3][1],matrix_[3][2]);

    // tien ve phia truoc thi tang,phia sau thi giam
    //cho nen m2 > -650, moi -320
    if(m[2] > -320.0f ){
        matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -5.0f));
    }
}

void Camera::setPos( const glm::vec3 &new_pos)
{
    matrix_ = glm::translate(matrix_,new_pos);
}

glm::vec3 Camera::getPos()
{
    glm::vec3 m = glm::vec3(matrix_[3][0],matrix_[3][1],matrix_[3][2]);
    return m;
}
