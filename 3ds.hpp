#ifndef _3DS_H
#define _3DS_H

#define PRIMARY       0x4D4D

#define OBJECTINFO    0x3D3D
#define VERSION       0x0002
#define EDITKEYFRAME  0xB000

#define MATERIAL      0xAFFF
#define OBJECT        0x4000

#define MATNAME       0xA000
#define MATDIFFUSE    0xA020
#define MATMAP        0xA200
#define MATMAPFILE    0xA300

#define OBJECT_MESH   0x4100

#define OBJECT_VERTICES     0x4110
#define OBJECT_FACES        0x4120
#define OBJECT_MATERIAL     0x4130
#define OBJECT_UV       0x4140

#include <vector>
#include <math.h>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdint.h>
#include <string.h>
#include <GL/gl.h>
#include <GL/glu.h>

using namespace std;

struct CVector3{
    float x,y,z;
};

struct CVector2{
    float x,y;
};


struct tFace
{
    int vertIndex[3];
    int coordIndex[3];
};


struct tMaterialInfo
{
    char  strName[255];
    char  strFile[255];
    uint8_t color[3];
    int   texureId;
    float uTile;
    float vTile;
    float uOffset;
    float vOffset;
};




struct t3DObject
{
    int  numOfVerts;
    int  numOfFaces;
    int  numTexVertex;
    int  materialID;
    bool bHasTexture;
    char strName[255];
    CVector3  *pVerts;
    CVector3  *pNormals;
    CVector2  *pTexVerts;
    tFace *pFaces;
};


struct t3DModel
{
    int numOfObjects;
    int numOfMaterials;
    vector<tMaterialInfo> pMaterials;
    vector<t3DObject> pObject;
};


struct tIndices {
    unsigned short a, b, c, bVisible;
};

struct tChunk
{
    unsigned short int ID;
    unsigned int length;
    unsigned int bytesRead;
};

class CLoad3DS
{
public:
    CLoad3DS();

    bool Import3DS(t3DModel *pModel, const char *strFileName);
    void loadModel(t3DModel g_3DModel,GLuint texture);

private:

    int GetString(char *);

    void ReadChunk(tChunk *);

    void ProcessNextChunk(t3DModel *pModel, tChunk *);

    void ProcessNextObjectChunk(t3DModel *pModel, t3DObject *pObject, tChunk *);

    void ProcessNextMaterialChunk(t3DModel *pModel, tChunk *);

    void ReadColorChunk(tMaterialInfo *pMaterial, tChunk *pChunk);

    void ReadVertices(t3DObject *pObject, tChunk *);

    void ReadVertexIndices(t3DObject *pObject, tChunk *);

    void ReadUVCoordinates(t3DObject *pObject, tChunk *);

    void ReadObjectMaterial(t3DModel *pModel, t3DObject *pObject, tChunk *pPreviousChunk);

    void ComputeNormals(t3DModel *pModel);

    void CleanUp();

    FILE *m_FilePointer;
};


#endif
