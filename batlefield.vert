varying vec4 TexCoord;
uniform sampler2D  texture_id2;  // height map
vec4 position;


void main(void)
{
    TexCoord = gl_MultiTexCoord0;

   position = gl_Vertex;
   position.y = (texture2D(texture_id2,TexCoord.xy).x * 200.0)- 170.0; //-300 de no xuong duoi, neu ko no o tren sapceship
   position.x *= 2.0;
   gl_Position = gl_ModelViewProjectionMatrix * position;

}
