
uniform samplerCube cubeMap;
varying vec3 view_vec;
varying vec3 obj_normal;


void main(void)
{

      vec3  fvNormal         = normalize(obj_normal);
      vec3  fvViewDirection  = normalize(view_vec);
      vec3  fvReflection     = normalize(reflect(fvViewDirection, fvNormal));
      gl_FragColor           = textureCube(cubeMap, fvReflection);


 // Reflective ray (when hitting the object)
// vec3 ref = reflect(-view_vec,obj_normal);

 // Cube map lookup
// vec3 color = textureCube(skybox, ref);

}



















