#ifndef _BULLET_HPP_
#define _BULLET_HPP_

#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"


class Bullet : public SceneObject
{
public:
    Bullet();
    ~Bullet();
    glm::vec3 getPos();
    void setPos(const glm::vec3 &pos2);
    void setDir(const glm::vec3 &dir2);
    void setColor(GLfloat r1,GLfloat g1, GLfloat b1);
    float getRadius();
    void setType(int type);
    int getType();
    glm::vec3 getDir();//get velocity
    float getMass();

protected:
    void privateInit();
    void privateRender();
    void privateUpdate();

private:

    //color
    GLfloat r,g,b;

    int list_id_;
    glm::vec3 dir_; //velocity
    glm::vec3 pos_;
    float radius_;
    int type_; // declare type of bullets that belong to which type of enemy
    float mass_;


};
#endif
