#ifndef _SPACESHIP_HPP_
#define _SPACESHIP_HPP_


#include "Shader.hpp" //phai dat truoc
#include"3ds.hpp"

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"


class SpaceShip : public SceneObject
{
public:
    SpaceShip();
    ~SpaceShip();
    void moveLeft();
    void moveRight();
    void moveForward();
    void moveBack();
    glm::vec3 getPos();
    void setPos(const glm::vec3 &new_pos);
    void setLife(float l1);
    float getRadius();
    float getLife();
    glm::vec3 getCameraPos();
    void setCameraPos(const glm::vec3 &pos);
    void setSkyboxTexture(GLuint texture);
    GLuint LoadTexture(const char *pszFilename);

protected:
    void privateInit();
    void privateRender();
    void privateUpdate();


private:
    float speed_;
    float life_;
    float armor_;  //ao giap

    glm::vec3 pos_; //position
    float radius_;
    int score_;
    GLuint list_id_;
    Shader s_;
    GLuint location_;//reflect  shader
    GLuint location1_;
    glm::vec3 camera_pos;
    GLuint cubeMap;

    // load 3d model
    CLoad3DS load_;
    t3DModel model_;
    GLuint texture_;

};

#endif
