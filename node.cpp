#include "node.h"

#include <QDebug>

Node::Node(){
    vertex = nullptr;

}

Node::Node(GMlib::TSVertex<float> *vertex1){
    vertex = vertex1;
}

//to return the edge between 2 verteces
GMlib::TSEdge<float> *Node::isNeighbour(Node &a)
{
    GMlib::Array<GMlib::TSEdge<float>* > edge_array = vertex->getEdges();//array of edges from vertex

    for(int i = 0 ; i < edge_array.getSize() ; i++)
        if(a.isVertex(*edge_array[i]->getOtherVertex(*vertex)))  // means a is another vertex of the edge from vertex
            return edge_array[i];

    return nullptr;
}

bool Node::isVertex(GMlib::TSVertex<float> &vertex1)
{
    return &vertex1 == vertex;
}


GMlib::Array<GMlib::TSTriangle<float>* > Node::getTriangle()
{
    return vertex->getTriangles();

}

GMlib::TSVertex<float> *Node::getVertex()
{
    return vertex;

}
//set z value of vertex
void Node::setZ(float z){
    vertex->setZ(z);
}
