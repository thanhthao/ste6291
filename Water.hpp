#ifndef _WATER_HPP_
#define _WATER_HPP_

#include "Shader.hpp" //phai dat truoc
#include <windows.h>
#include "GL/glew.h"
#include "GL/freeglut.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include "SceneObject.hpp"
#include "glm/glm.hpp"

class Water : public SceneObject
{
public:
    Water();
    ~Water();
    void setPos(GLfloat newpos);

protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate();

private:
    std::vector< glm::vec3 > vertexArray_; // Maybe two-dim vector and several arrays
    std::vector< unsigned int> indexArray_; // index arrays
    // normal array.

    std::vector<glm::vec2> texCoordArray_; // texture coord array
    GLuint textureId_;
    Shader s_;
    GLint texSampler_;
    GLfloat waterTime_;
    GLint waveTimeLoc_;
    GLfloat pos_;//to change position
};

#endif
