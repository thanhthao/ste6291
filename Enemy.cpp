#include "Enemy.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/ext.hpp"  // to print matrix_
#include <iostream>
#include "SOIL.h"


Enemy::Enemy()
{
    // create a random number
    //The time function returns the number of seconds elapsed since midnight (00:00:00)
    //, January 1, 1970, coordinated universal time (UTC), according to the system clock.

    srand(time(0));
    random_ = rand()%321 - 200;// random number must be in (-320,320)
    live_ = true;


    if (random_ <= 0.0f){

        radius_ = 20.0f;
        life_   = 5.0f;
        mass_   = 1.0f;
        pos_  = glm::vec3(random_, 0.0f, -1050.0f);
    }
    else if(random_ > 30.0f && random_ <= 70.0f){

        radius_ = 25.0f;
        life_   = 10.0f;
        mass_   = 2.0f;
        pos_  = glm::vec3(random_, 0.0f, -1850.0f);
    }
    else {
        radius_ = 30.0f;
        life_   = 15.0f;
        mass_   = 3.0f;
        pos_  = glm::vec3(random_, 0.0f, -3850.0f);
    }

    privateInit();

}

Enemy::~Enemy()
{
}

// create geometry/display list
void Enemy::privateInit()
{
    angle_ = 0.0f;

    list_id_ = glGenLists(1);
    glNewList(list_id_, GL_COMPILE);

    if(radius_== 20.0f){

        //draw star
        glBegin(GL_LINE_LOOP);

        glColor3f(0.6f,0.6f,0.6f);
        glVertex3f(20.0f,120.0f,0.0f);
        glVertex3f(180.0f,120.0f,0.0f);
        glVertex3f(45.0f,20.0f,0.0f);
        glVertex3f(100.0f,190.0f,0.0f);
        glVertex3f(155.0f,20.0f,0.0f);
        glEnd();


    }
    else if(radius_ == 25.0f){

        // pyramid
        glBegin( GL_TRIANGLES );
        glColor3f( 1.0f, 0.0f, 0.0f ); glVertex3f( 0.0f, 1.f, 0.0f );
        glColor3f( 0.0f, 1.0f, 0.0f ); glVertex3f( -1.0f, -1.0f, 1.0f );
        glColor3f( 0.0f, 0.0f, 1.0f ); glVertex3f( 1.0f, -1.0f, 1.0f);

        glColor3f( 1.0f, 0.0f, 0.0f ); glVertex3f( 0.0f, 1.0f, 0.0f);
        glColor3f( 0.0f, 1.0f, 0.0f ); glVertex3f( -1.0f, -1.0f, 1.0f);
        glColor3f( 0.0f, 0.0f, 1.0f ); glVertex3f( 0.0f, -1.0f, -1.0f);

        glColor3f( 1.0f, 0.0f, 0.0f ); glVertex3f( 0.0f, 1.0f, 0.0f);
        glColor3f( 0.0f, 1.0f, 0.0f ); glVertex3f( 0.0f, -1.0f, -1.0f);
        glColor3f( 0.0f, 0.0f, 1.0f ); glVertex3f( 1.0f, -1.0f, 1.0f);


        glColor3f( 1.0f, 0.0f, 0.0f ); glVertex3f( -1.0f, -1.0f, 1.0f);
        glColor3f( 0.0f, 1.0f, 0.0f ); glVertex3f( 0.0f, -1.0f, -1.0f);
        glColor3f( 0.0f, 0.0f, 1.0f ); glVertex3f( 1.0f, -1.0f, 1.0f);

        glEnd();

    }

    else if(radius_== 30.0f) {

        //pentagon
        glColor3f(0.8f,0.8f,0.8f);
        glBegin( GL_TRIANGLES );
        glVertex3f(2.5f, 2.5f, -25.0f);
        glVertex3f(7.5f, 2.5f, -25.0f);
        glVertex3f(2.5f, 5.0f, -25.0f);

        glVertex3f(2.5f, 5.0f, -25.0f);
        glVertex3f(7.5f, 2.5f, -25.0f);
        glVertex3f(7.5f, 5.0f, -25.0f);

        glVertex3f(2.5f, 5.0f, -25.0f);
        glVertex3f(7.5f, 5.0f, -25.0f);
        glVertex3f(5.0f, 7.5f, -25.0f);
        glEnd();
    }

   // glFlush();
    glEndList();


}

// draw object/call list
void Enemy::privateRender()
{


    if(radius_==20.0f)
    {
        glScalef(0.35f,0.35f,0.0f) ;
    }
    else if(radius_==25.0f)
    {
        glScalef(20.0f,20.0f,20.0f) ;
    }
    else if(radius_==30.0f)

        glScalef(20.0f,20.0f,20.0f) ;

    glCallList(list_id_) ;


}

// translate object
void Enemy::privateUpdate()
{
    if(live_==true){

        if(radius_ == 20.0f ){
            angle_  = angle_ + 0.005;
            pos_[0] = random_ + glm::cos(angle_)* 300.0f;    // direction from left to right
            pos_[2] = pos_[2]+ 0.3f;
            matrix_ = glm::translate(glm::mat4(), pos_);

        }
        else {

            angle_  = angle_ + 0.005;
            pos_[0] = random_ + glm::sin(angle_)* 300.0f;   // direction from right to left
            pos_[2] = pos_[2]+ 0.3f;
            matrix_ = glm::translate(glm::mat4(), pos_);


        }

    }
}

glm::vec3 Enemy::getPos()
{

    return pos_;
}

float Enemy::getRadius()
{

    return radius_;
}

float Enemy::getLife()
{
    return life_;
}

void Enemy::setLife(float life)
{

    life_= life;
}

float Enemy::getMass()
{
    return mass_;
}

glm::vec3 Enemy::getDir()
{
    return dir_;
}

void Enemy::setDir(const glm::vec3 &dir)
{
    dir_= dir;
}

void Enemy::setLive(bool value)
{
    live_= value;
}
void Enemy::setPos(const glm::vec3 &pos_new)
{
    pos_= pos_new;
}
