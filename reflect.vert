
uniform vec3 camera_pos;
varying vec3 view_vec;
varying vec3 obj_normal;
//uniform samplerCube cubeMap;


void main(void)
{
// calculate view_vec and obj_normal

    gl_Position = ftransform();
  //  vec4 object_pos = gl_ModelViewMatrix * gl_Vertex;
      vec4 object_pos = gl_Vertex; // do this in world space,not in object space

    view_vec   = vec3(camera_pos - object_pos.xzy);
    obj_normal = gl_NormalMatrix * gl_Normal;


}






