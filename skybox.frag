uniform samplerCube skybox;
varying  vec4 texCoord;

void main(void)
{   
   //gl_FragColor = vec4( textureCube(skybox,texCoord.xyz), 0.0 );
    vec3 cube= vec3 (textureCube(skybox,texCoord.xyz));
   gl_FragColor = vec4( cube, 0.0 );
}
