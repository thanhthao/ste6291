#include "tree.hpp"
#include "glm/ext.hpp"  // to print matrix_
#include "glm/gtx/string_cast.hpp"
#include <GL/glut.h>
#include <iostream>
#include "SOIL/SOIL.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define ANGLE 0.3
#define SCALE 0.9
#define RAND 0.06

//---------------------------------------
// Calculate random value between [-R..R]
//---------------------------------------


Tree::Tree()
{

}

Tree::~Tree()
{
}

float Tree::myrand(float R)
{
   return (2 * R * rand()) / RAND_MAX - R;
}

void Tree::tree(float x1, float y1, float length1, float angle1, int depth)
{
   if (depth > 0)
   {
      // Draw line segment
      float x2 = x1 + length1 * cos(angle1);
      float y2 = y1 + length1 * sin(angle1);
      glVertex2f(x1, y1);
      glVertex2f(x2, y2);

      // Make two recursive calls
      float length2 = length1 * (SCALE + myrand(RAND));
      float angle2 = angle1 + ANGLE + myrand(RAND);
      tree(x2, y2, length2, angle2, depth-1);
      length2 = length1 * (SCALE + myrand(RAND));
      angle2 = angle1 - ANGLE + myrand(RAND);
      tree(x2, y2, length2, angle2, depth-1);
   }
}
void Tree::privateInit()
{
    // Initialize OpenGL
       glClearColor(0.0, 0.0, 0.0, 1.0);
       glMatrixMode(GL_PROJECTION);
       glLoadIdentity();
       float radius = 100;
       glOrtho(-radius, radius, -radius, radius, -radius, radius);
}

void Tree::privateRender()
{
    // Draw tree
       glClear(GL_COLOR_BUFFER_BIT);
       glColor3f(0.0, 1.0, 0.0);
       glBegin(GL_LINES);
       tree(0, -80, 20, 1.5, 10);
       glEnd();
       glFlush();


       glEnable(GL_PRIMITIVE_RESTART);


       glActiveTexture(GL_TEXTURE0);
       glEnable(GL_TEXTURE_2D);
       glBindTexture(GL_TEXTURE_2D, texture_id);


       glEnableClientState(GL_VERTEX_ARRAY);                  // enable vertex arrays
       glEnableClientState(GL_TEXTURE_COORD_ARRAY);

       glTexCoordPointer(2, GL_FLOAT, 0, &texCoordArray_[0]);
       glVertexPointer(3, GL_FLOAT, 0, &vertexArray_[0]);     // set vertex pointer

       glDrawElements(GL_TRIANGLE_STRIP, indexArray_.size(), GL_UNSIGNED_INT,&indexArray_[0]); // connect points

       glDisableClientState(GL_VERTEX_ARRAY);                 // disable vertex arrays
       glDisableClientState(GL_TEXTURE_COORD_ARRAY);

       glDisable(GL_PRIMITIVE_RESTART);


       glActiveTexture(GL_TEXTURE0);
       glDisable(GL_TEXTURE_2D);


}

void Tree::privateUpdate()
{

}

