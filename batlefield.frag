uniform sampler2D  texture_id1,texture_id3;  // color and light
varying vec4 TexCoord;
uniform float blendFactor;
void main(void)
{   

   //gl_FragColor = gl_Color * texture2D(tex, gl_TexCoord[0].xy);


    //float x= 64;
    //float z=256;


    vec4 text1 = texture2D(texture_id1,TexCoord.xy);
    vec4 text3 = texture2D(texture_id3,TexCoord.xy);


    gl_FragColor  = mix(text1,text3,blendFactor);

    // gl_FragColor  = texture2D(texture_id3,TexCoord.xy);

}


