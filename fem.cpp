#include "fem.h"
#include <QDebug>
#include <gmCoreModule>


FEMApp::FEMApp(int d){

    update = 0.0;
    move_up = true;
}

FEMApp::FEMApp(const ArrayLX<GMlib::TSVertex<float> >& v){

    update = 0.0;
    move_up= true;
}


void FEMApp::regularNode(int m, int n, float radius){

    GMlib::ArrayLX<GMlib::TSVertex<float> > pt;     //an array of all verteces

    const GMlib::Point<float,2> center(0.0, 0.0);  //position for the center node

      //m: number of level (circle)
      //n: number of points in the first circle
      //radius: the radius of the big circle

    pt.setSize(1 + n*(m*(m+1))/2); // number of nodes in the outer circle will be equal the number of the inner circle
                                   // + the number of nodes in the first circle
                                   // means: n + 2*n + 3*n +...+m*n=n*(1+2+3+...+m)=n*(m*(m+1)/2)
                                   //and +1 for the center node

    pt[0].setPos(center);
    GMlib::Vector<float,2> v1(1,0);
    GMlib::Vector<float,2> v2(0,1);
    GMlib::Vector<float,2> v3;
    int k = 0; //index of array pt[]

    for(int j = 0 ; j < m ; j++){                                  // for each circle
                                                                   // number of nodes in each circle = the order of circle * number of nodes in the first cirlce
        for(int i = 0 ; i < n*(j+1); i++){                         // go through all nodes in each circle

                                                                   // we go j from index 0 =>number of nodes in j-th circle= n*(j+1)
            GMlib::Angle a = GMlib::Angle(i * M_2PI/(n*(j+1)));
            GMlib::SqMatrix<float,2> mt(a, v1, v2);                // Make a rotation matrix in the plane spanned by vectors v1 and v2
            v3 = GMlib::Vector<float,2>(((j+1)*radius)/m,0);       // (j+1)*radius/m  is radius of j-th circle
            pt[k++].setPos(mt * v3);                               // vector v3 always in x-axis, rotate with angle a to create the new nodes
        }
    }

    insertAlways(pt);
}

void FEMApp::internalNode(){  // make the internal nodes
                              // check if not the boundary node , insert to node_array

    int number = 0;
    for(int i = 0 ; i < this->getSize() ; i++){  //compute the size of node_array without boundary nodes
        if(!(*this)[i].boundary())
            number++;
    }

    node_array.setSize(number);
    int index = 0;                               // neu ko,khi them vao node_array ko lien tuc, bi loi
                                                 // make sure the index of node_array is continous
    for(int i = 0 ; i < this->getSize() ; i++){
        if(!(*this)[i].boundary())                      // go for all nodes
            node_array[index++]= Node(getVertex(i)) ;   //if not in boundary ,insert to node_array
    }


}

void FEMApp::randomNode(int k,float radius)
{   // k: number of points in the boundary
    //first, make the boundary
    GMlib::ArrayLX<GMlib::TSVertex<float> > vt;
    GMlib::Vector<float,2> v1(1.0,0.0);
    GMlib::Vector<float,2> v2(0.0,1.0);

    float inner_radius;
    float inter_point;       // number of internal point
    float number_triangle;   //number of triangle
    float min_dist ;
    int   m = 0;             // index of vt[]

    float b = M_PI * radius * radius;     // area of the big circle
    float a = ( M_2PI * radius) / k;      // the length of 1 side of the triangle
    float c = sqrt(3)/4 * a * a;          // area of triangle

    inner_radius    = sqrt(radius * radius - (a/2)*(a/2));
    number_triangle = int( b/c);
    inter_point     = 1.0f + (int(float(number_triangle - k )/2.0f + 0.5f)); // round int(float....

    qDebug() << "inner_radius: " << inner_radius << " number_triangle: " << number_triangle << " internal points: " << inter_point;

    vt.setSize(k + inter_point);
    qDebug()<<"number of triangle:"<< number_triangle;
    qDebug()<<"number of internal point:"<< inter_point;

    GMlib::Random<float> rand(-radius,radius);

    for(int i = 0 ; i < k ; i++)
    {
        GMlib::Angle a = i * M_2PI / k;
        GMlib::SqMatrix<float,2> mt(a,v1,v2);
        GMlib::Point<float,2> point = mt * GMlib::Vector<float,2>(radius,0);
        vt[m++].setPos(GMlib::TSVertex<float>(point));

    }

    GMlib::Point<float,2> pt;
    // min_dist = std::sqrt(4 /std::sqrt(3)* b /number_triangle)/1.7; // 1.5 or 1.7 or 2 ,check ???

    //we want to create equilateral triangles(tam giac deu)
    // area of 1 triangle = total area / number of triangle = sqrt(3)/4* bow(x,2) ,
    // with x : the length of 1 side of triangle
    //
    min_dist = 2*std::sqrt(b/(std::sqrt(3)* number_triangle))/1.5; //check ???

    qDebug() << "Minimal distance: " << min_dist;

    for(int j = 0; j < inter_point ; j++ ){

        do {
            do{

                pt = GMlib::Point<float,2>(rand.get(),rand.get());

            } while(pt.getLength() > inner_radius);

        }while (!pointOK(pt,vt,min_dist));

     //new point pt must have the length < inner_radius
     //and the distance from pt to all setting points > min_dis

      vt[k+j].setPos(pt);
      qDebug()<<"pt :" << pt;

    }

    qDebug()<<"vt[]:"<< vt;

    insertAlways(vt);

}


void FEMApp::computeMatrix()               //compute stiffness matrix
{
    A.setDim(node_array.size(),node_array.size());

    // calculate A[i][j],not diagonal
    for(int i =0 ; i < node_array.size(); i++) {
        for (int j = i+1 ; j < node_array.size(); j++ ){

            GMlib::TSEdge<float>* edge = node_array[i].isNeighbour(node_array[j]);//find edge between 2 nodes

            if( edge!= nullptr)
            {
                float val = findElement(edge); //get 2 triangles, calculate the value
                A[i][j] = A[j][i] = val;
            }
            else
                A[i][j] = A[j][i] = 0  ;// not neighbour,dont need to calculate,will be = 0
        }
    }

    // calculate a[i][i] in diagonal

    for(int i = 0 ; i < node_array.size(); i++) {

        GMlib::Array<GMlib::TSTriangle<float>* > triangle_array = node_array[i].getTriangle();
        GMlib::Vector<GMlib::Vector<float,2>,3> d;
        float diag = 0.0f;

        for (int k = 0; k < triangle_array.size(); k++)
        {
            // find
            d = findVectors(node_array[i],triangle_array[k]);

            diag +=(d[2]*d[2]) / (2*std::abs(d[0] ^ d[1]));
        }

        A[i][i] = diag;

    }

    qDebug() <<"stiffness matrix:"<< A;
    A1 = A.invert();
    qDebug() <<"invert matrix:"<< A1;

}

void FEMApp::computeLoadVector()
{
    b.setDim(node_array.size());
    //GMlib::Vector<float,2> d1,d2;

    for(int i = 0 ; i < node_array.size(); i++ ){
        float volume2 = 0;
        GMlib::Array<GMlib::TSTriangle<float>* > tri_array = node_array[i].getTriangle();

        for(int j = 0; j < tri_array.size() ; j++){

            volume2 += tri_array[j]->getArea2D()/3;   // volume = 1/3 * area of the base * height
                                                      // height=1
            // another way to compute the volume as the theory
            //            GMlib::Array<GMlib::TSVertex<float> *> ver_array = tri_array[j]->getVertices();
            //            // check position of p0,p1,p2
            //            for(int k = 0; k < ver_array.size(); k++){
            //                if(ver_array[k] == node_array[i].getVertex())
            //                    std::swap(ver_array[k],ver_array[0]);          //to make sure in the right position
            //                d1 = ver_array[2]->getParameter() - ver_array[0]->getParameter();
            //                d2 = ver_array[1]->getParameter() - ver_array[0]->getParameter();
            //                volume += std::abs(d1^d2)/6;
            //            }

        }

        b[i] = volume2 ;
        qDebug() << b[i];
    }

    // qDebug()<<"load vector:" << b;
}

GMlib::Vector<GMlib::Vector<float,2> ,3> FEMApp::findVectors(GMlib::TSEdge<float> *edge)
{
    GMlib::Point<float,2> p0 = edge->getFirstVertex()->getParameter(); // get the first point p0 of the edge
    GMlib::Point<float,2> p1 = edge->getLastVertex()->getParameter();  //get the last poit p1 of the edge
    GMlib::Array<GMlib::TSTriangle<float>*> triangle_array = edge->getTriangle(); // get 2 triangles have the same edge "edge"
    GMlib::Point<float,2> p2,p3;

    GMlib::Array<GMlib::TSVertex<float>*> ver_triangle1 = triangle_array[0]->getVertices(); //get 3 vertices of triangle 1
    GMlib::Array<GMlib::TSVertex<float>*> ver_triangle2 = triangle_array[1]->getVertices();//get 3 vertices of triangle 2

    //  find p2 and p3
    for(int j = 0 ; j < 3; j++){

        if(ver_triangle1[j] != edge->getFirstVertex() && ver_triangle1[j] != edge->getLastVertex())
            p2 = ver_triangle1[j]->getParameter();
        if(ver_triangle2[j] != edge->getFirstVertex() && ver_triangle2[j] != edge->getLastVertex())
            p3 = ver_triangle2[j]->getParameter();
    }

    GMlib::Vector<GMlib::Vector<float,2> ,3> d;
    //  qDebug()<<"points: "<< p0 <<","<< p1 <<","<< p2 <<" ,"<<p3;
    d[0] = p1 - p0;
    d[1] = p2 - p0;
    d[2] = p3 - p0;
    return d;

}

GMlib::Vector<GMlib::Vector<float, 2>, 3> FEMApp::findVectors(Node &n, GMlib::TSTriangle<float> *triangle)
{
    GMlib::Array<GMlib::TSVertex<float>*> vertex_array = triangle->getVertices(); //get 3 vertices of triangle

    // check that if node n = vertex_array[0] ok
    // if not swap these to make sure them in the correct position
    if(n.isVertex(*vertex_array[1])){
        std::swap(vertex_array[0],vertex_array[1]);
        std::swap(vertex_array[1],vertex_array[2]);
    }
    if(n.isVertex(*vertex_array[2])){
        std::swap(vertex_array[0],vertex_array[2]);
        std::swap(vertex_array[1],vertex_array[2]);
    }

    GMlib::Point<float,2> p0, p1, p2;

    p0 = vertex_array[0]->getParameter();
    p1 = vertex_array[1]->getParameter();
    p2 = vertex_array[2]->getParameter();

    GMlib::Vector<GMlib::Vector<float,2>,3> d;
    d[0] = p1 - p0;
    d[1] = p2 - p0;
    d[2] = p2 - p1;

    return d;
}


float FEMApp::findElement(GMlib::TSEdge<float> *edge)
{
    GMlib::Vector<GMlib::Vector<float,2>,3> d;
    d = findVectors(edge);
    float dd = 1.0f / (d[0] * d[0]);

    //triangle 1
    float area1 = std::abs(d[0] ^ d[1]);
    float dh1   = dd * (d[1] * d[0]);
    float h1    = dd * area1 * area1;

    //triangle 2
    float area2 = std::abs(d[0] ^ d[2]);
    float dh2   = dd * (d[2] * d[0]);
    float h2    = dd * area2 * area2;

    return ((dh1*(1.0f-dh1)/h1) -dd) * (area1/2.0f) + ((dh2*(1.0f-dh2))/h2 -dd) * (area2/2.0f) ;
}

void FEMApp::updateNode(double d){    //set the z-value fof the internal nodes

    GMlib::DVector<float> x;
    x = A1 * (d*b);
    for(int i = 0; i < node_array.size(); i++){
        node_array[i].setZ(x[i]);
        //qDebug()<<"node after update:"<< node_array[i].getVertex()->getParameter();
    }
}

void FEMApp::localSimulate(double dt){


    if(move_up)
        update += dt;
    else
        update -= dt;

    if(update > 2)
        move_up = false;

    if(update < -2)
        move_up = true;

    updateNode(update);
    this->replot();
    computeNormals();

}



bool FEMApp::pointOK(GMlib::Point<float,2> pt, GMlib::ArrayLX<GMlib::TSVertex<float> > vt, float min_dist)
{
    for(int i = 0 ; i < vt.size() ; i++){
        float length = (pt - vt[i].getParameter()).getLength();
        if(length < min_dist)
            return false;
    }
    return true;
}


