////////////////////////////////////////////////////////////////////////////
//
// Copyright 1993-2014 NVIDIA Corporation.  All rights reserved.
//
// Please refer to the NVIDIA end user license agreement (EULA) associated
// with this source code for terms and conditions that govern your use of
// this software. Any use, reproduction, disclosure, or distribution of
// this software and related documentation outside the terms of the EULA
// is strictly prohibited.
//
////////////////////////////////////////////////////////////////////////////

/*
    This example demonstrates how to use the Cuda OpenGL bindings to
    dynamically modify a vertex buffer using a Cuda kernel.

    The steps are:
    1. Create an empty vertex buffer object (VBO)
    2. Register the VBO with Cuda
    3. Map the VBO for writing from Cuda
    4. Run Cuda kernel to modify the vertex positions
    5. Unmap the VBO
    6. Render the results using OpenGL

    Host code
*/

#include <cuda_runtime.h>
#include <math.h>

inline __device__ float3 operator*(float3 a, float b)
{
    return make_float3(a.x * b, a.y * b, a.z * b);
}

inline __device__ float3 operator-(float3 a, float3 b)
{
    return make_float3(b.x - a.x, b.y - a.y, b.z - a.z);
}

inline __device__ float3 cross(float3 a, float3 b)
{
    return make_float3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}

inline __device__ float dot(float3 a, float3 b)
{
    return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline __device__ float3 normalize(float3 v)
{
    float invLen = 1.0f / sqrtf(dot(v, v));
    return v * invLen;
}

__global__ void position_kernel(float4 *pos, float4 *in_current_pos, float4 *in_minus1_pos, unsigned int width, unsigned int height)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

    // calculate uv coordinates
    float u = x / (float) width;
    float v = y / (float) height;
    u = u*2.0f - 1.0f;
    v = v*2.0f - 1.0f;

    float w = 0.0f;

    if(x >= height-2 || y >= width-2 || x<=1 || y <= 1)
        w = in_current_pos[y*width+x].y;
    else
        w = 2.0f*in_current_pos[y*width + x].y -
            in_minus1_pos[y*width + x].y +
            0.5f*(in_current_pos[(y+1)*width + x].y +
                  in_current_pos[(y-1)*width + x].y +
                  in_current_pos[y*width + (x-1)].y +
                  in_current_pos[y*width + (x+1)].y -
                  4.0f*in_current_pos[y*width + x].y);



    pos[y*width + x] = make_float4(pos[y*width + x].x,w,pos[y*width + x].z,1.0f);

}

__global__ void normal_kernel(float3 *out_normals, float4 *in_pos, unsigned int width, unsigned int height)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

    // calculate uv coordinates
    float u = x / (float) width;
    float v = y / (float) height;
    u = u*2.0f - 1.0f;
    v = v*2.0f - 1.0f;

    if(x >= height-2 || y >= width-2 || x<=1 || y <= 1)
        out_normals[y*width + x] = make_float3(0,1,0);
    else
    {
        float3 pos3 = make_float3(in_pos[(y+1)*width + x].x,in_pos[(y+1)*width + x].y,in_pos[(y+1)*width + x].z);
        float3 pos2 = make_float3(in_pos[y*width + x+1].x,in_pos[y*width + x+1].y,in_pos[y*width + x+1].z);
        float3 pos1 = make_float3(in_pos[y*width + x].x,in_pos[y*width + x].y,in_pos[y*width + x].z);

        float3 vec1 = pos1 - pos2;
        float3 vec2 = pos1 - pos3;

        float3 crossProduct = cross(vec1,vec2);

        float3 normalized = normalize(crossProduct);

        out_normals[y*width + x] = make_float3(normalized.x,normalized.y,normalized.z);
    }
}

extern "C"
void launch_kernel(float3 *out_normals, float4 *out_pos,float4 *in_current_pos,float4 *in_minus1_pos, unsigned int mesh_width, unsigned int mesh_height)
{
    // execute the kernel
    dim3 block(8, 8, 1);
    dim3 grid(mesh_width / block.x, mesh_height / block.y, 1);
    position_kernel<<< grid, block >>> (out_pos, in_current_pos,in_minus1_pos, mesh_width, mesh_height);
    normal_kernel<<< grid, block >>> (out_normals, out_pos, mesh_width, mesh_height);
}
