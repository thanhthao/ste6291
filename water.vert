
uniform float waveTime;
uniform float waveWidth;
uniform float waveHeight;
varying vec3 TexCoord;
void main(void)
{

    TexCoord = gl_MultiTexCoord0.xzy;
    float waveFreq = 0.1;
    vec4 v = vec4(gl_Vertex);
    v.y = sin(waveWidth * v.x + waveTime) * cos(waveWidth * v.z + waveTime) * waveHeight - 75.0;
    gl_Position = gl_ModelViewProjectionMatrix * v;
  // TexCoord = gl_MultiTexCoord0;
}
