#include "Skybox.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/ext.hpp"  // to print matrix_
#include <iostream>
#include "SOIL.h"


Skybox::Skybox()
{
    pos_    = glm::vec3(100.0f, 0.0f, -400.0f);

}

Skybox::~Skybox()
{
}

// create geometry/display list
void Skybox::privateInit()
{


    s_.initShaders("./shaders/skybox");      // skybox : ten trung ben shader file



    int img_height1 = 512;
    int img_width1  = 512 ;
    unsigned char* down = SOIL_load_image("./images/skybox/colorMap2012_2.bmp", &img_width1, &img_height1, NULL, 0);

    GLuint text_ = SOIL_load_OGL_texture
            (
                "./images/skybox/skybox_down.bmp",
                SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y
                );
    if(text_ == 0)
        std::cout<< "SOIL loading error " ;
    else
        std::cout<< "SOIL loading ok" <<std::endl;

    unsigned char* east = SOIL_load_image("./images/skybox/skybox_east.bmp", &img_width1, &img_height1, NULL, 0);
    unsigned char* west = SOIL_load_image("./images/skybox/skybox_west.bmp", &img_width1, &img_height1, NULL, 0);
    unsigned char* up = SOIL_load_image("./images/skybox/skybox_up.bmp", &img_width1, &img_height1, NULL, 0);
    unsigned char* south = SOIL_load_image("./images/skybox/skybox_south.bmp", &img_width1, &img_height1, NULL, 0);
    unsigned char* north = SOIL_load_image("./images/skybox/skybox_north.bmp", &img_width1, &img_height1, NULL, 0);

    //cube map here ,ten la skybox_ nhung dung la cube map
    // tao function reflect nhu trong slide cho doi tuong nhu spaceship giong nhu vi du hinh tron
    // repeat the landscape, non stop.
    glGenTextures(1, &skybox_);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox_);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0,GL_RGBA, img_width1, img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, east);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0,GL_RGBA, img_width1, img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, west);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0,GL_RGBA, img_width1, img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, up);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0,GL_RGBA, img_width1, img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, down);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0,GL_RGBA, img_width1, img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, north);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0,GL_RGBA, img_width1, img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, south);


    float  size = 1.0f;   // should be unit =1
    listId_ = glGenLists(1);
    glNewList(listId_, GL_COMPILE);
    glBegin(GL_QUADS);

    // Near Face

    glColor3f(1.0f, 0.0f, 0.0f);
    glNormal3f( 0.0f, 0.0f, 1.0f);
    glVertex3f(-size, -size,  size);
    glVertex3f( size, -size,  size);
    glVertex3f( size,  size,  size);
    glVertex3f(-size,  size,  size);

    // right Face
    glColor3f(1.0f, 1.0f, 0.0f);
    glNormal3f( 0.0f, 0.0f, 1.0f);
    glVertex3f(size, -size,  -size);
    glVertex3f( size, size,  -size);
    glVertex3f( size,  size,  size);
    glVertex3f(size,  -size,  size);

    // left Face
    glColor3f(1.0f, 0.0f, 1.0f);
    glNormal3f( 0.0f, 0.0f, 1.0f);
    glVertex3f(-size,  -size,  -size);
    glVertex3f(-size, -size,  size);
    glVertex3f( -size, size, size);
    glVertex3f( -size,  size,  -size);

    // back Face
    glColor3f(0.0f, 1.0f, -1.0f);
    glNormal3f( 0.0f, 0.0f, -1.0f);
    glVertex3f( -size,  -size,  -size);
    glVertex3f( -size, size,  -size);
    glVertex3f( size, size,  -size);
    glVertex3f( size,  -size,  -size);

    // upper Face
    glColor3f(-1.0f,-1.0f, 1.0f);
    glNormal3f( 0.0f, 0.0f, 1.0f);
    glVertex3f( size,  size,  size);
    glVertex3f( size, size,  -size);
    glVertex3f( -size, size,  -size);
    glVertex3f( -size,  size,  size);

    // bottom Face
    glColor3f(-1.0f, -1.0f, 1.0f);
    glNormal3f( 0.0f,0.0f, 1.0f);
    glVertex3f( size,  -size,  size);
    glVertex3f(-size, -size,  size);
    glVertex3f( -size, -size, -size);
    glVertex3f(size,  -size, -size);

    glEnd();

    glEndList();

    s_.enable();

    tex_ = glGetUniformLocation(s_.getProg(), "skybox");
    glUniform1i(tex_, 0)   ;

    s_.disable();


}

// draw object/call list
void Skybox::privateRender()
{
    s_.enable();

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_CUBE_MAP);
    glBindTexture(GL_TEXTURE_CUBE_MAP,skybox_);

    glFrontFace(GL_CW);
    glDisable(GL_DEPTH_TEST);

    glCallList(listId_);

    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_CUBE_MAP);

    glFrontFace(GL_CCW);
    glEnable(GL_DEPTH_TEST);


    s_.disable();



}

// translate object
void Skybox::privateUpdate()
{

}
void Skybox::setPosition(const glm::vec3 &pos){

    matrix_ = glm::mat4();
    matrix_ = glm::translate(matrix_, pos);
}

GLuint Skybox::getCubeMap(){

    return skybox_ ;
}
