#ifndef NODE_H
#define NODE_H

#include <gmTriangleSystemModule>


class Node{

    private:
    GMlib::TSVertex<float>* vertex;

    public:
    Node();
    Node(GMlib::TSVertex<float>* vertex1);
    GMlib::TSEdge<float> * isNeighbour( Node &a);
    bool isVertex(GMlib::TSVertex<float> &vertex1);
    GMlib::Array<GMlib::TSTriangle<float>* > getTriangle();
    GMlib::TSVertex<float> *getVertex();
    void setZ(float z);
};




#endif // NODE_H
