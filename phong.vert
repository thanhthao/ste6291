

varying vec3 Normal;

void main()
{
    Normal = gl_NormalMatrix * gl_Normal;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    gl_FrontColor = gl_Color;
}

//in vec3 vp; // positions from mesh
//in vec3 vn; // normals from mesh
//uniform mat4 P, V, M; // proj, view, model matrices
//out vec3 pos_eye;
//out vec3 n_eye;

//void main () {
//  pos_eye = vec3 (V * M * vec4 (vp, 1.0));
//  n_eye = vec3 (V * M * vec4 (vn, 0.0));
//  gl_Position = P * V * M * vec4 (vp, 1.0);
//}
