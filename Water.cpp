#include "Water.hpp"
#include "glm/ext.hpp"  // to print matrix_
#include "glm/gtx/string_cast.hpp"
#include <GL/glut.h>
#include <iostream>
#include "SOIL/SOIL.h"

Water::Water()
{
    waterTime_ = 0.5;
    pos_= 64;
}

Water::~Water()
{
}

void Water::privateInit()
{
    s_.initShaders("./shaders/water");  // water:ten of shader file


    glPrimitiveRestartIndex(32768);//= 64*512

    // color

    textureId_ = SOIL_load_OGL_texture
            (
                "./images/water1.jpg",
                SOIL_LOAD_AUTO,
                SOIL_CREATE_NEW_ID,
                SOIL_FLAG_INVERT_Y
                );
    if(textureId_ == 0)
        std::cout<<"SOIL loading error for water " ;
    else
        std::cout<<" load water ok " ;

    int img_height1;
    int img_width1;
    unsigned char* img1 = SOIL_load_image("./images/water1.jpg", &img_width1, &img_height1, NULL, 0);

    glGenTextures(1, &textureId_);

    glBindTexture(GL_TEXTURE_2D, textureId_);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP);

    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA_FLOAT32_ATI, img_width1, img_height1, 0, GL_RGB, GL_UNSIGNED_BYTE, img1);



    // Create vertex arrays
    int x_size = 50;
    int z_size = 32;  // giam chieu z con 1/8

    glm::vec3 v;
    glm::vec2 t;

    for(int x = 0 ; x < x_size ; x ++){
        for(int z = 0 ; z < z_size ; z ++){

            v[0]= x*10.0f - 320.0f; //appear in  the middle of the screen -320
            v[1]= 0;
            v[2]= ( z + pos_)*(-10.0f);
            t[0]= x/float(x_size-1);
            t[1]= (z)/float(z_size-1);
            vertexArray_.push_back(v);
            texCoordArray_.push_back(t);

        }
    }


    // create index array
    for(int x = 0 ; x < x_size - 1 ; x ++){
        for(int z = 0 ; z < z_size ; z ++){
            indexArray_.push_back(z + z_size*x);               // x =0: 0,1,2,3,4  cot 1
            indexArray_.push_back(z + z_size*x + z_size );     // x =0: 5,6,7,8,9  cot 2
        }

        indexArray_.push_back(32768);
    }

    indexArray_.pop_back();



    s_.enable();

    texSampler_ = glGetUniformLocation(s_.getProg(), "texture_id"); // name of texture o tren, phai cung ten voi file fragment
    glUniform1i(texSampler_, 0)   ; // 0 : level 0 gan voi texture_id


    waveTimeLoc_ = glGetUniformLocation(s_.getProg(), "waveTime");
   // glUniform1f(waveTimeLoc, waveTime);
    glUniform1f(waveTimeLoc_, waterTime_); // 0.5 truyen gia tri vao bien waveTimeLoc

    GLint waveWidthLoc = glGetUniformLocation(s_.getProg(), "waveWidth");
    glUniform1f(waveWidthLoc, 0.7);

    GLint waveHeightLoc = glGetUniformLocation(s_.getProg(), "waveHeight");
    glUniform1f(waveHeightLoc, 10.0);

    s_.disable();

}

void Water::privateRender()
{

    s_.enable();

   waterTime_ += 0.005 ;
   glUniform1f(waveTimeLoc_, waterTime_);
    // Render the battlefield
    glEnable(GL_PRIMITIVE_RESTART);


    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, textureId_);


    glEnableClientState(GL_VERTEX_ARRAY);                  // enable vertex arrays
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glTexCoordPointer(2, GL_FLOAT, 0, &texCoordArray_[0]);
    glVertexPointer(3, GL_FLOAT, 0, &vertexArray_[0]);     // set vertex pointer

    glDrawElements(GL_TRIANGLE_STRIP, indexArray_.size(), GL_UNSIGNED_INT,&indexArray_[0]); // connect points

    glDisableClientState(GL_VERTEX_ARRAY);                 // disable vertex arrays
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);

    glDisable(GL_PRIMITIVE_RESTART);


    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_2D);


    s_.disable();



}

void Water::privateUpdate()
{

}

void Water::setPos(GLfloat newpos)
{
    pos_= newpos;
}

